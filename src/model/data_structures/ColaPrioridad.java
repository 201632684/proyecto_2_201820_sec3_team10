package model.data_structures;



public class ColaPrioridad <T extends Comparable>{

	private T[] pq;
	private int N;

	public void crearCP(int max) {
		pq = (T[]) new Comparable[max];
	}

	public boolean esVecia() {
		return N == 0;
	}

	public void agregar(T elemento) {
		pq[N++] = elemento;
	}

	public T max() {
		int max = 0;
		for (int i = 1; i < N; i++)
			if (less(max, i))
				max = i;
		exch(max, N - 1);
		return pq[--N];

	}
	
	public int darNumElementos(){
		return N;
	}
	
	public int tamanoMax(){
		return pq.length;
	}

	private boolean less(int a, int b) {
		return (pq[a].compareTo(pq[b])) < 0;
	}

	private void exch(int a, int b) {
		T temp = pq[a];
		pq[a] = pq[b];
		pq[b] = temp;
	}
}
