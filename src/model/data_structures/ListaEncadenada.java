package model.data_structures;



import java.util.Iterator;

import javax.print.attribute.standard.Sides;
import javax.swing.plaf.metal.MetalBorders.ToggleButtonBorder;

public class ListaEncadenada<k> implements Iterable {

	private NodoSencillo<k> primera = null;
	private NodoSencillo<k> ultimo = null;
	int listSize = darNumeroElementos();
	public Object cargue;

	public Iterator<k> iterator() {
		Iterator<k> it = new MiIterator<k>(primera);
		return it;
	}
	public class MiIterator <K> implements Iterator <K>  {

		NodoSencillo<K> actual;

		public MiIterator (NodoSencillo<K> PprimerElemento){
			actual = PprimerElemento; 
		}

		@Override
		public boolean hasNext() {

			if(darNumeroElementos() == 0){
				return false;
			}

			if(actual == null)
				return true;
			return actual.siguiente != null;
		}

		@Override
		public K next() {
			if(actual == null)
				actual = (NodoSencillo<K>) primera;
			else{
				actual = actual.siguiente;
			}
			return actual.elemento;
		}

	}



	public void agregarElementoFinal(k elem) {

		NodoSencillo<k> agregar = new NodoSencillo<k>(elem);

		if(primera == null){
			primera = agregar;
			ultimo = primera;
		}else{
			NodoSencillo<k> anteriorUltimo = ultimo;
			ultimo = agregar;
			anteriorUltimo.siguiente = ultimo;
		}
	}

	public NodoSencillo<k> darElemento(int pos) {

		NodoSencillo<k> temp = primera;
		int cont = 0;
		while(temp.siguiente != null && cont <= pos-1 )
		{
			temp = temp.siguiente;
			cont++;
		}

		return temp;
	}

	public k darElementoT(int pos) {

		NodoSencillo<k> actual = primera;
		for(;pos > 0 && actual != null; pos--)
			actual = actual.siguiente;
		return actual == null ? null : actual.elemento; 
	}

	public k darElementoFinal()
	{
		return darElementoT(darNumeroElementos()-1);
	}


	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		NodoSencillo<k> actual = primera;
		int pos = 0;
		while(actual!=null && actual.elemento != null)
		{
			pos++;
			actual=actual.siguiente;
		}
		return pos;

	}



	/**
	 * Elimina dada una posicion
	 */
	public k remove(int posicion){

		if(primera == null)
			throw new IndexOutOfBoundsException();

		else{
			NodoSencillo<k> actual = primera;
			NodoSencillo<k> anterior = null;

			if(posicion == 0){
				primera = actual.siguiente;
				return primera.elemento;
			}

			else
			{
				while(actual != null  && posicion > 0)
				{
					anterior = actual;
					actual = actual.siguiente;
					posicion --;					
				}

				if(posicion > 0)
					throw new IndexOutOfBoundsException();

				else
				{
					if(actual != null){
						anterior.siguiente = actual.siguiente;//Revisar en caso de error
						return actual.elemento;
					}


					else
						anterior.siguiente = null;
					return anterior.elemento;


				}	
			}
		}

	}



	public static class NodoSencillo <K>
	{
		K elemento;
		NodoSencillo<K> siguiente;
		public NodoSencillo(K elemento) {
			this.elemento = elemento;
		}
	}
}
