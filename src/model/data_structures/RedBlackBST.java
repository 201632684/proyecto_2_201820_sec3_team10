package model.data_structures;



import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDateTime;
import java.util.Iterator;

public class RedBlackBST <K extends Comparable, V> implements Iterable<K>
{
	private Node root;
	private static final boolean RED=true;
	private static final boolean BLACK=false;
	private class Node
	{
		private Node right;
		private Node left;
		private boolean color;
		private int N;
		private K key;
		private V value; 
		public Node(K k, V v, int n, boolean c)
		{
			this.key=k;
			this.value=v;
			this.N=n;
			this.color=c;
		}
	}
	public Node getRoot()
	{
		return root;
	}
	private boolean isRed(Node x)
	{
		if(x==null) return false;
		else return x.color==RED;
	}
	Node rotateLeft(Node h)
	{
		Node x=h.right;
		h.right=x.left;
		x.left=h;
		x.color=h.color;
		h.color=RED;
		x.N=h.N;
		h.N=1+size(h.left)+size(h.right);
		return x;
	}
	Node rotateRight(Node h)
	{
		Node x= h.left;
		h.left=x.right;
		x.right=h;
		x.color=h.color;
		h.color=RED;
		x.N=h.N;
		h.N= 1+ size(h.left)+size(h.right);
		return x;
	}
	void flipColors(Node x){
		x.color=RED;
		x.left.color=BLACK;
		x.right.color=BLACK;
	}
	public int size()
	{
		return size(root);
	}
	private int size(Node x){
		if(x==null) return 0;
		else return x.N;
	}
	public boolean isEmpty(){
		if(root==null)return true;
		else return false;
	}	
	public V get(String s){
		return get(s);
	}
	
	public V get1(LocalDateTime fechaTerminacion){
		return get1(fechaTerminacion);
	}
	private V get(Node x,LocalDateTime fechaTerminacion){
		if(x==null) return null;
		else if(fechaTerminacion.compareTo((ChronoLocalDateTime<?>) x.key)>0) return get(x.right,fechaTerminacion);
		else if(fechaTerminacion.compareTo((ChronoLocalDateTime<?>) x.key)<0) return get(x.left,fechaTerminacion);
		else return x.value;
	}
	private Node get1(Node x,LocalDateTime localDateTime){
		if(x==null) return null;
		else if(localDateTime.compareTo((ChronoLocalDateTime<?>) x.key)>0) return (RedBlackBST<K, V>.Node) get(x.right,localDateTime);
		else if(localDateTime.compareTo((ChronoLocalDateTime<?>) x.key)<0) return (RedBlackBST<K, V>.Node) get(x.left,localDateTime);
		else return (RedBlackBST<K, V>.Node) x.value;
	}
	public boolean contains(K k)
	{
		return contains(k,root);
	}
	private boolean contains(K k, Node x)
	{
		if(x==null) return false;
		else if(k.compareTo(x.key)>0) return contains(k,x.right);
		else if(k.compareTo(x.key)<0) return contains(k,x.left);
		else  return true;
	}
	public void put(LocalDateTime localDateTime, V v){
		root=put(root,localDateTime,v);
		root.color=BLACK;
	}
	public Node put(Node x,LocalDateTime localDateTime,V v)
	{
		if(x==null)return new Node((K) localDateTime,v,1, RED);
		int comp=localDateTime.compareTo((ChronoLocalDateTime<?>) x.key);
		if(comp<0) x.left=put(x.left,localDateTime,v);
		else if(comp>0) x.right=put(x.right,localDateTime,v);
		else x.value=v;		
		if(isRed(x.right)&& !isRed(x.left)) x=rotateLeft(x);
		if(isRed(x.left)&& isRed(x.left.left))  x=rotateRight(x);
		if(isRed(x.left)&&isRed(x.right)) flipColors(x);
		
		x.N=size(x.left)+size(x.right)+1;
		return x;
	}
	//TODO
	public void deleteMin(){
		root=deleteMin(root);
	}
	//TODO
	private Node deleteMin(Node x)
	{
		if(x.left==null)return x.right;
		x.left=deleteMin(x.left);
		x.N=size(x.left)+size(x.right)+1;
		return x;
	}
	//TODO
	public void deleteMax(){
		deleteMax(root);		
	}
		//TODO
	private Node deleteMax(Node x)
	{
		if(x.right==null)return x.left;
		x.right= deleteMax(x.right);
		x.N=size(x.left)+size(x.right)+1;
		return x;
	}
	//TODO
	public void delete(K k){
		root=delete(k, root);
	}
	private Node delete(K k,Node x)
	{
		return null;
	}
	//TODO
	public int height(){
		return height(root);
	}
	//TODO
	private int height(Node x){
	    if(x==null) return 0;
		else if(x.right==null) return 1;
		else return 1+height(x.right);
	}
	public K min()
	{		
		if(root==null)return null;
		else return min(root).key;
	}
	private Node min(Node x)
	{
		if(x==null) return null;
		else if(x.left!=null)return min(x.left);
		else return x;
	}
	public K max()
	{	
		if(root==null)return null;
		else return max(root).key;
	}
	private Node max(Node x)
	{
		if (x==null) return null;
		else if(x.right!=null)return max(x.right);
		else return x;
	}
	
	//TODO
	public boolean check(){
		return false;
	}
	
	@Override
	public Iterator<K> iterator() {
		return new Iterator<K>(){
			private Node actual=min(root);
			private Node end=max(root);
			private int m=0;
			
			private Node updateNext(){
				if(!hasNext()) return null;
				if(actual.right!=null)return min(actual.right);
				
				K llaveActual=actual.key;
				Node post=null;
				Node tempRaiz=root;
				while(tempRaiz!=null)
				{
					if(llaveActual.compareTo(tempRaiz.key)<0)
					{
						post=tempRaiz;
						tempRaiz=tempRaiz.left;
					}
					else if(llaveActual.compareTo(tempRaiz.key)>0)
					{
						tempRaiz=tempRaiz.right;
					}
					else
					{
						actual=post;
						break;
					}
				}
				return post;
			}
		
			@Override
			public boolean hasNext() {
				if(root==null)return false;
				else{
					int x= actual.key.compareTo(end.key);
					if( end!=null &&actual!=null&&x<0) return true;
					else return false;
				}
			}

			@Override
			public K next() {
				if(m==0)
				{	
					m++;
					return actual.key;
				}
				else if(actual.right!=null) 
				{
					actual=min(actual.right);
					return actual.key;
				}
				else
				{
					actual=updateNext();
					return actual.key;
				}
			
			}
			
		};
	}
}
