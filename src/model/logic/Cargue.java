package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;

import model.data_structures.EncadenamientoSeparadoTH;
import model.data_structures.ListaEncadenada;
import model.data_structures.RedBlackBST;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

public class Cargue {
	
	private RedBlackBST<String,ListaEncadenada<Trip>> arbolTrips;
	private EncadenamientoSeparadoTH<Integer,Station> tablaEstacion;
	private ListaEncadenada<Bike> listaBikes;

	private static final int CAPACIDAD = 100000;
	
	public Cargue() {
		arbolTrips = new RedBlackBST<>();
		tablaEstacion = new EncadenamientoSeparadoTH<>(CAPACIDAD);
		listaBikes = new ListaEncadenada<>();
	}
	
	/*
	 * Lectura de arhivos Tipo 1
	 */
	
	public void leerCSVT1(String ruta) throws Exception
	{
		try (BufferedReader br = new BufferedReader(new FileReader(ruta))) 
		{
			br.readLine();
			for(String s = br.readLine(); s != null; s = br.readLine())
			{
				String[] datos = s.replaceAll(",,", ", , ").replaceAll("\"\"", " ").replaceAll("\"", "").split(",");
				Trip trip = crerTrip(datos);
				agregarArbol(trip);
			}
		}
	}
	
	private void agregarArbol(Trip trip)
	{
		ListaEncadenada<Trip> lista = arbolTrips.get1(trip.getStopTime());
		if(lista == null) {
			lista = new ListaEncadenada<Trip>();
			arbolTrips.put(trip.getStopTime(), lista);
		}
		lista.agregarElementoFinal(trip);
	}
	
	private Trip crerTrip(String[] datos) throws ParseException
	{
		long tripId = Long.parseLong(datos[0]);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		
		LocalDateTime tiempoIninio=LocalDateTime.parse(datos[1]);
		LocalDateTime tiempoFinal=LocalDateTime.parse(datos[2]);
		
		int bikeId = Integer.parseInt(datos[3]);
		int tripDuration = Integer.parseInt(datos[4]);
		int startStationId = Integer.parseInt(datos[5]);
		String nameStart = datos[6];
		int endStationId = Integer.parseInt(datos[7]);
		String nameEnd = datos[8];
		String userType = datos[9];
		String gender = datos[10];
		if(gender.equals(" "))
			gender = Trip.UNKNOWN;
		int birthyear = -1;
		if(!datos[11].equals(" "))
			birthyear = Integer.parseInt(datos[11]);
		long segViaje = generarSegundos(datos[1],datos[2]);
		return new Trip(tripId, tiempoIninio, tiempoFinal, bikeId, tripDuration, startStationId, 
				nameStart, endStationId, nameEnd, userType, gender, birthyear, segViaje);
	}
	
	private long generarSegundos(String fecha1, String fecha2) throws ParseException 
	{
		SimpleDateFormat  formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date f1 =  formatter.parse(fecha1);
		Date f2 =  formatter.parse(fecha2);
		return f2.getSeconds() - f1.getSeconds();
		
	}
	
	/*
	 * Lectura de arhivos Tipo 2
	 */
	
	public void leerCSVT2(String ruta) throws Exception
	{
		try (BufferedReader br = new BufferedReader(new FileReader(ruta))) 
		{
			br.readLine();
			for(String s = br.readLine(); s != null; s = br.readLine())
			{
				String[] datos = s.replaceAll(",,", ", , ").replaceAll("\"", "").split(",");
				Station station = crearEstacion(datos);
				agregarHash(station);
			}
		}
	}
	
	private void agregarHash(Station station)
	{
		tablaEstacion.insertar(station.getStationId(), station);
	}
	
	private Station crearEstacion(String[] datos)
	{
		int stationId = Integer.parseInt(datos[0]);
		String stationName = datos[1];
		String city = datos[2];
		double latitud = Double.parseDouble(datos[3]);
		double longitud = Double.parseDouble(datos[4]);
		int capacidad = Integer.parseInt(datos[5]);
		String onlineDato = datos[6];
		return new Station(stationId, stationName, onlineDato, city, 
				latitud, longitud, capacidad);
	}
	
	public RedBlackBST<String, ListaEncadenada<Trip>> getArbolTrips() {
		return arbolTrips;
	}
	
	public ListaEncadenada<Bike> getListaBikes() {
		return listaBikes;
	}
	 
	public EncadenamientoSeparadoTH<Integer, Station> getTablaEstacion() {
		return tablaEstacion;
	}
	
	
	
	/*if (arbolTrips == null ) 
		{
			String[ ] Trips = {TRIPS_Q1};  //Array de 4 elementos

			for (int i = 0; i < Trips.length; i++) {
				String ruta= Trips[i];


				// TODO Auto-generated method stub
				try {
					BufferedReader leer= new BufferedReader(new FileReader(ruta));
					leer.readLine();
					for(String s = leer.readLine(); s != null;s =leer.readLine()) {

						String[] datos = s.replaceAll(",,", ", , ").replaceAll("\"", "").split(",");
						long id;
						for (int j = 0; j < datos.length; j++) {
							datos[i] = datos[i].trim();
						}
						System.out.println(Arrays.toString(datos));
						String Str = datos[0];
						id = Long.parseUnsignedLong(Str);

						DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");


						String tiempoIn=(datos[1]);
						String tiempoFinal=(datos[2]);


						Double bikeId= Double.parseDouble(datos[3]);
						Double tripDuration= Double.parseDouble(datos[4]);
						int startStationId=Integer.parseInt(datos[5]);
						String nameStation= datos[6];
						int endStationId=Integer.parseInt(datos[7]);

						String gender = datos[10];

						//System.out.println(getToStation+"vamos a ver si entra");

						if (gender.equals(" ")) {
							gender = "ng";
							Trip viaje= new Trip(id, tiempoIn, tiempoFinal, bikeId, tripDuration, startStationId, endStationId, gender);

						}
						else
						{
							gender= datos[10];
							Trip viaje= new Trip(id, tiempoIn, tiempoFinal, bikeId, tripDuration, startStationId, endStationId, gender);
							arbolTrips.put(datos[2],viaje );
						}
						//VOByke cicla= new VOByke(bikeId);

					}




					leer.close();

				} catch (Exception e) {

					e.printStackTrace();
				}

			}

			//TODO Crear Lista Trips
		}

		if (tablaEstacion==null) 
		{ 
			String[ ] Staciones = {STATIONS_Q1_Q2,STATIONS_Q3_Q4,};  //Array de 2 elementos


			for (int i = 0; i < Staciones.length; i++) {
				String ruta= Staciones[i];

				// TODO Auto-generated method stub
				try {
					BufferedReader leer= new BufferedReader(new FileReader(ruta));
					leer.readLine();
					for(String s = leer.readLine(); s != null;s =leer.readLine()) {

						String[] datos = s.replaceAll(",,", ", , ").split(",");
						int stationId = Integer.parseInt(datos[0]);

						String stationName= (datos[1]);
						String city=(datos[2]);
						double latitud= Double.parseDouble(datos[3]);
						double longitud= Double.parseDouble(datos[4]);
						String capacidad = datos[5];
						String onlineDato =datos[6];	
						//LocalDateTime startDate= LocalDateTime.parse(datos[6]);


						Station viaje= new Station(stationId, stationName, city, latitud, longitud, capacidad, onlineDato);
						tablaEstacion.insertar(stationId, viaje);

						//VOByke cicla= new VOByke(bikeId);

					}
					leer.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}*/
}
