package model.logic;

import API.IManager;
import controller.Controller.ResultadoCampanna;
import model.data_structures.EncadenamientoSeparadoTH;
import model.data_structures.ICola;
import model.data_structures.ILista;

import model.data_structures.ListaEncadenada;
import model.data_structures.RedBlackBST;
import model.vo.Bike;
import model.vo.BikeRoute;
import model.vo.Station;
import model.vo.Trip;

import java.io.BufferedReader;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Iterator;

import javax.swing.text.html.parser.ParserDelegator;

public class Manager implements IManager {

	// -----------------------T1----------------------------
	// Ruta del archivo de datos 2017-Q1
	public static final String TRIPS_Q1 = "./data/Divvy_Trips_2017_Q1.csv";

	// Ruta del archivo de trips 2017-Q2
	public static final String TRIPS_Q2 = "./data/Divvy_Trips_2017_Q2.csv";

	// Ruta del archivo de trips 2017-Q3
	public static final String TRIPS_Q3 = "./data/Divvy_Trips_2017_Q3.csv";

	// Ruta del archivo de trips 2017-Q4
	public static final String TRIPS_Q4 = "./data/Divvy_Trips_2017_Q4.csv";

	// -----------------------T2----------------------------
	// Ruta del archivo de stations 2017-Q1-Q2
	public static final String STATIONS_Q1_Q2 = "./data/Divvy_Stations_2017_Q1Q2.csv";

	// Ruta del archivo de stations 2017-Q3-Q4
	public static final String STATIONS_Q3_Q4 = "./data/Divvy_Stations_2017_Q3Q4.csv";

	private Cargue cargue;

	public Manager() {
		cargue = new Cargue();
	}

	public void cargarDatos(String rutaTrips, String rutaStations, String dataBikeRoutes) {
		try
		{
//			long time = System.currentTimeMillis();
			cargue.leerCSVT1(rutaTrips);
			int c = 0;
			for(String s : cargue.getArbolTrips()) {
				ListaEncadenada<Trip> l = cargue.getArbolTrips().get(s);
				Iterator<Trip> it = l.iterator();
				while (it.hasNext()){
					c++;
					System.out.println(c);
				}
			}
			System.out.println("Datos: "+c);
			cargue.leerCSVT2(rutaStations);
//			System.out.println("Datos: "+cargue.getTablaEstacion().size());
//			System.out.println("Time: "+(System.currentTimeMillis() - time));
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		new Manager().cargarDatos(TRIPS_Q1, STATIONS_Q3_Q4, "");
	}

	@Override
	public ICola<Trip> A1(int n, LocalDateTime fechaTerminacion) {
	ListaEncadenada<Trip> hola= cargue.getArbolTrips().get1(fechaTerminacion);
	hola.iterator().has
	
		
	
	Station hola = cargue.getTablaEstacion().hashCode(n)
		return null;
	}

	@Override
	public ILista<Trip> A2(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Trip> A3(int n, LocalDate fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Bike> B1(int limiteInferior, int limiteSuperior) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Trip> B2(LocalDateTime fechaInicial, LocalDateTime fechaFinal, int limiteInferiorTiempo,
			int limiteSuperiorTiempo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		// TODO Auto-generated method stub
		return new int[] { 0, 0 };
	}

	@Override
	public ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double[] C2(int LA, int LO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int darSector(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ILista<Station> C3(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<BikeRoute> C4(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<BikeRoute> C5(double latitudI, double longitudI, double latitudF, double longitudF) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ICola<Trip> A1(int n, LocalDate fechaTerminacion) {
		// TODO Auto-generated method stub
		return null;
	}
}
