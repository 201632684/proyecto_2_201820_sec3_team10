package model.vo;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Trip implements Comparable<Trip> {

    public final static String MALE = "Male";
    public final static String FEMALE = "Female";
    public final static String UNKNOWN = "Unknown";

    private long tripId;
    private LocalDateTime startTime;
    private LocalDateTime stopTime;
    
    private int bikeId;
    private int tripDuration;
    private int startStationId;
    private String nameStart;
    private int endStationId;
    private String nameEnd;
    private String userType;
    private String gender;
    private int birthyear;
    private long segViaje;
    
    

    

    public Trip(long tripId, LocalDateTime tiempoIninio, LocalDateTime tiempoFinal, int bikeId, int tripDuration, int startStationId,
			String nameStart, int endStationId, String nameEnd, String userType, String gender, int birthyear, long segViaje) {
		this.tripId = tripId;
		this.startTime = tiempoIninio;
		this.stopTime = tiempoFinal;
		this.bikeId = bikeId;
		this.tripDuration = tripDuration;
		this.startStationId = startStationId;
		this.nameStart = nameStart;
		this.endStationId = endStationId;
		this.nameEnd = nameEnd;
		this.userType = userType;
		this.gender = gender;
		this.birthyear = birthyear;
		this.segViaje = segViaje;
	}

	@Override
    public int compareTo(Trip o) {
    	// TODO completar
        return 0;
    }
	
	public long getSegViaje() {
		return segViaje;
	}

	public void setSegViaje(long segViaje) {
		this.segViaje = segViaje;
	}
	
    public long getTripId() {
        return tripId;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getStopTime() {
        return stopTime;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTripDuration() {
        return tripDuration;
    }

    public int getStartStationId() {
        return startStationId;
    }

    public int getEndStationId() {
        return endStationId;
    }

    public String getGender() {
        return gender;
    }

	public String getNameStart() {
		return nameStart;
	}

	public void setNameStart(String nameStart) {
		this.nameStart = nameStart;
	}

	public String getNameEnd() {
		return nameEnd;
	}

	public void setNameEnd(String nameEnd) {
		this.nameEnd = nameEnd;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public int getBirthyear() {
		return birthyear;
	}

	public void setBirthyear(int birthyear) {
		this.birthyear = birthyear;
	}

	public static String getMale() {
		return MALE;
	}

	public static String getFemale() {
		return FEMALE;
	}

	public static String getUnknown() {
		return UNKNOWN;
	}

	public void setTripId(long tripId) {
		this.tripId = tripId;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public void setStopTime(LocalDateTime stopTime) {
		this.stopTime = stopTime;
	}

	public void setBikeId(int bikeId) {
		this.bikeId = bikeId;
	}

	public void setTripDuration(int tripDuration) {
		this.tripDuration = tripDuration;
	}

	public void setStartStationId(int startStationId) {
		this.startStationId = startStationId;
	}

	public void setEndStationId(int endStationId) {
		this.endStationId = endStationId;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
    
    
}
