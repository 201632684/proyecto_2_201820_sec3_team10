package model.vo;

public class Station implements Comparable<Station> {
	
	private int stationId;
	private String stationName;
	private String onlineDato;
	private String city;
	private double latitud;
	private double longitud;
	private int capacidad;
	
	public Station(int stationId, String stationName, String onlineDato, String city, double latitud, double longitud,
			int capacidad) {
		super();
		this.stationId = stationId;
		this.stationName = stationName;
		this.onlineDato = onlineDato;
		this.city = city;
		this.latitud = latitud;
		this.longitud = longitud;
		this.capacidad = capacidad;
	}

	public int getStationId() {
		return stationId;
	}

	public void setStationId(int stationId) {
		this.stationId = stationId;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getOnlineDato() {
		return onlineDato;
	}

	public void setOnlineDato(String onlineDato) {
		this.onlineDato = onlineDato;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLongitud() {
		return longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	public int getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	@Override
	public int compareTo(Station arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
}
